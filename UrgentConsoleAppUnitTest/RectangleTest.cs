﻿using System;
using NUnit.Framework;
using UrgentConsoleApp;

namespace UrgentConsoleAppUnitTest
{
    [TestFixture]
    public class RectangleTest
    {
        [Test]
        public void createlList_hasValues_returnsMaterialList()
        {
            //arrange
            Rectangle sut = new Rectangle
            {
                PositionX = 10,
                PositionY = 10,
                Width = 30,
                Height = 40
            };
            //act
            string result = sut.CreateList();
            //assert
            Assert.AreEqual(result, "Rectangle (10,10) width=30 height=40");
        }
        [Test]
        public void validate_validInput_returnsTrue()
        {
            //arrange
            Rectangle sut = new Rectangle
            {
                PositionX = 10,
                PositionY = 10,
                Width = 30,
                Height = 40
            };
            //act
            bool result = sut.Validate();
            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void validate_invalidInput_returnsFalse()
        {
            //arrange
            Rectangle sut = new Rectangle
            {
                PositionX = -10,
                PositionY = 10,
                Width = 30,
                Height = 40
            };
            //a
            bool result = sut.Validate();
            //assert
            Assert.IsFalse(result);
        }
    }
}
