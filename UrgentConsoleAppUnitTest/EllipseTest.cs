﻿using System;
using NUnit.Framework;
using UrgentConsoleApp;

namespace UrgentConsoleAppUnitTest
{
    [TestFixture]
    public class EllipseTest
    {
        [Test]
        public void createlList_hasValues_returnsMaterialList()
        {
            //arrange
            Ellipse sut = new Ellipse
            {
                PositionX = 100,
                PositionY = 150,
                HorizontalDiameter = 300, 
                VerticalDiameter= 200
            };
            //act
            string result = sut.CreateList();
            //assert
            Assert.AreEqual(result, "Ellipse (100,150) diameterH = 300 diameterV = 200");
        }
        [Test]
        public void validate_validInput_returnsTrue()
        {
            //arrange
            Ellipse sut = new Ellipse
            {
                PositionX = 100,
                PositionY = 150,
                HorizontalDiameter = 300,
                VerticalDiameter = 200
            };
            //act
            bool result = sut.Validate();
            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void validate_invalidInput_returnsFalse()
        {
            //arrange
            Ellipse sut = new Ellipse
            {
                PositionX =-100,
                PositionY = 150,
                HorizontalDiameter = 300,
                VerticalDiameter = 200
            };
            //a
            bool result = sut.Validate();
            //assert
            Assert.IsFalse(result);
        }
    }
}
