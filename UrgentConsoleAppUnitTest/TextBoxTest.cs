﻿using System;
using NUnit.Framework;
using UrgentConsoleApp;

namespace UrgentConsoleAppUnitTest
{
    [TestFixture]
    public class TextBoxTest
    {
        [Test]
        public void createlList_hasValues_returnsMaterialList()
        {
            //arrange
            TextBox sut = new TextBox
            {
                PositionX = 5,
                PositionY = 5,
                Width = 200,
                Height = 100,
                Text = @"sample text"
            };
            //act
            string result = sut.CreateList();
            //assert
            Assert.AreEqual(result, "Textbox (5,5) width=200 height=100 text=\"sample text\"" );
        }
        [Test]
        public void validate_validInput_returnsTrue()
        {
            //arrange
            TextBox sut = new TextBox
            {
                PositionX = 10,
                PositionY = 10,
                Width = 30, 
                Height = 10
            };
            //act
            bool result = sut.Validate();
            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void validate_invalidInput_returnsFalse()
        {
            //arrange
            TextBox sut = new TextBox
            {
                PositionX = -10,
                PositionY = 10,
                Width = 30
            };
            //act
            bool result = sut.Validate();
            //assert
            Assert.IsFalse(result);
        }
    }
}
