﻿using System;
using NUnit.Framework;
using UrgentConsoleApp;

namespace UrgentConsoleAppUnitTest
{
    [TestFixture]
    public class CircleTest
    {
        [Test]
        public void createlList_hasValues_returnsMaterialList()
        {
            //arrange
            Circle sut = new Circle
            {
                PositionX = 1,
                PositionY = 1,
                Diameter = 300
            };
            //act
            string result = sut.CreateList();
            //assert
            Assert.AreEqual(result, "Circle (1,1) size=300");
        }
        [Test]
        public void validate_validInput_returnsTrue()
        {
            //arrange
            Circle sut = new Circle
            {
                PositionX = 1,
                PositionY = 1,
                Diameter = 300
            };
            //act
            bool result = sut.Validate();
            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void validate_invalidInput_returnsFalse()
        {
            //arrange
            Circle sut = new Circle
            {
                PositionX = -1,
                PositionY = 1,
                Diameter = 300
            };
            //a
            bool result = sut.Validate();
            //assert
            Assert.IsFalse(result);
        }
    }
}
