﻿using System;
using NUnit.Framework;
using UrgentConsoleApp;

namespace UrgentConsoleAppUnitTest
{
    [TestFixture]
    public class SquareTest
    {
        [Test]
        public void createlList_hasValues_returnsMaterialList()
        {
            //arrange
            Square sut = new Square
            {
                PositionX = 15,
                PositionY = 30,
                Width = 35
            };
            //act
            string result = sut.CreateList();
            //assert
            Assert.AreEqual(result, "Square (15,30) size=35");
        }
        [Test]
        public void validate_validInput_returnsTrue()
        {
            //arrange
            Square sut = new Square
            {
                PositionX = 10,
                PositionY = 10,
                Width = 30
            };
            //act
            bool result = sut.Validate();
            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void validate_invalidInput_returnsFalse()
        {
            //arrange
            Square sut = new Square
            {
                PositionX = -10,
                PositionY = 10,
                Width = 30
            };
            //act
            bool result = sut.Validate();
            //assert
            Assert.IsFalse(result);
        }
    }
}
