﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UrgentConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle = new Rectangle { PositionX = -10, PositionY = 10, Width = 30, Height = 40 }; //abort test
            //Rectangle rectangle = new Rectangle { PositionX =10, PositionY = 10, Width = 30, Height = 40 };//Normal output
            Square square = new Square { PositionX = 15, PositionY = 30, Width = 35 };
            Ellipse ellipse = new Ellipse { PositionX = 100, PositionY = 150, HorizontalDiameter = 300, VerticalDiameter = 200 };
            Circle circle = new Circle { PositionX = 1, PositionY = 1, Diameter = 300 };
            TextBox textBox = new TextBox { PositionX = 5, PositionY = 5, Width = 200, Height = 100, Text = "sample text" };

            List<Widget> widgets = new List<Widget> { rectangle, square, ellipse, circle, textBox };
            BillOfMaterialsBuilder billOfMaterialsBuilder = new BillOfMaterialsBuilder
            {
                Materials = widgets
            };

            ListBuilderDirector director = new ListBuilderDirector();
            director.Construct(billOfMaterialsBuilder);
            List<string> documnent = billOfMaterialsBuilder.GetDocument();
            documnent.ForEach(line => Console.WriteLine(line));
        }
    }
}