﻿using System;
using System.Collections.Generic;

namespace UrgentConsoleApp
{
    public interface IListBuilder
    {
        List<string> Document { get; set; }
        void MakeHeader();
        void MakeBody();
        void MakeFooter();
        List<string> GetDocument();
    }
}