﻿namespace UrgentConsoleApp
{
    public class Square : Widget
    {
        public int Width { get; set; }
        protected override string MakeMaterialList()
        {
            return $"Square ({PositionX},{PositionY}) size={Width}";
        }
        public new bool Validate()
        {
            return base.Validate() && Width >= 0;
        }
    }
}