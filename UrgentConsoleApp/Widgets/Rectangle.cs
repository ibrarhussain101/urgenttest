﻿namespace UrgentConsoleApp
{
    public class Rectangle : Square
    {
        public int Height { get; set; }

        protected override string MakeMaterialList()
        {
            return $"Rectangle ({PositionX},{PositionY}) width={Width} height={Height}";
        }
        public new bool Validate()
        {
            return base.Validate() && Height >= 0;
        }
    }

}