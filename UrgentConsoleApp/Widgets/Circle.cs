﻿namespace UrgentConsoleApp
{
    public class Circle : Widget
    {
        public int Diameter { get; set; }
        protected override string MakeMaterialList()
        {
            return $"Circle ({PositionX},{PositionY}) size={Diameter}";
        }

        public new bool Validate()
        {
            return base.Validate() && Diameter >= 0;
        }
    }
}