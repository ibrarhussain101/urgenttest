﻿namespace UrgentConsoleApp
{
    public class Ellipse : Widget
    {
        public int HorizontalDiameter { get; set; }
        public int VerticalDiameter { get; set; }
        protected override string MakeMaterialList()
        {
            return $"Ellipse ({PositionX},{PositionY}) diameterH = {HorizontalDiameter} diameterV = {VerticalDiameter}";
        }
        public new bool Validate()
        {
            return base.Validate() && HorizontalDiameter >= 0 &&  VerticalDiameter >= 0;
        }
    }
}