﻿namespace UrgentConsoleApp
{
    public abstract class Widget
    {
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        protected abstract string MakeMaterialList();
        public bool Validate()
        {
            return PositionX > 0 && PositionY > 0;
        }

        public string CreateList()
        {
            if (Validate())
            {
                return MakeMaterialList();
            }
            else
            {
                throw new System.Exception("Invalid widget: " + this.ToString());
            }
        }
        
    }
}