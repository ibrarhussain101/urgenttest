﻿namespace UrgentConsoleApp
{
    public class TextBox : Rectangle
    {
        public string Text { get; set; }
        protected override string MakeMaterialList()
        {
            return $"Textbox ({PositionX},{PositionY}) width={Width} height={Height} text=\"{Text}\"";
        }

    }
}