﻿namespace UrgentConsoleApp
{
    public class ListBuilderDirector
    {
        public void Construct(IListBuilder listBuilder)
        {
            listBuilder.MakeHeader();
            listBuilder.MakeBody();
            listBuilder.MakeFooter();
        }
    }
}