﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UrgentConsoleApp
{
    public class BillOfMaterialsBuilder : IListBuilder
    {
        public List<string> Document { get; set; } = new List<string>();

        public List<Widget> Materials { get; set; } = new List<Widget>();
        public bool abort = false;
        private const string lineDelimiter = "----------------------------------------------------------------";
        public void MakeBody()
        {
            try
            {
                Materials.ForEach(Materials => Document.Add(Materials.CreateList()));
            }
            catch (Exception e)
            {
                Document = new List<string> { "+++++Abort+++++" };
                abort = true;
                Log(e.Message + "\n" + e.StackTrace.ToString());                
            }
        }


        public void MakeFooter()
        {
            if (!abort) Document.Add(lineDelimiter);
        }

        public void MakeHeader()
        {
            Document.Add(lineDelimiter);
            Document.Add("Bill of Materials");
            Document.Add(lineDelimiter);
        }
        protected void Log(string message)
        {
            StreamWriter streamWriter;

            if (!File.Exists("logfile.txt"))
            {
                streamWriter = new StreamWriter("logfile.txt");
            }
            else
            {
                streamWriter = File.AppendText("logfile.txt");
            }

            streamWriter.WriteLine(DateTime.Now);
            streamWriter.WriteLine(message);
            streamWriter.WriteLine();

            // Close the stream:
            streamWriter.Close();
        }

        public List<string> GetDocument()
        {
            return Document;
        }
    }
}